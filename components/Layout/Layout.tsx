import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import Router from 'next/router'

type LayoutProps = {
  children: any
  title?: string
  description?: string
  backButton?: boolean
}

export default function Layout({
  children,
  title,
  description,
  backButton = false
}: LayoutProps) {
  return (
    <div className="container">
      <Head>
        <title>{title || 'Hacker TypeNext'}</title>
        <meta name="description" content={description} />
      </Head>
      <header>
        <nav>
          {backButton && (
            <span onClick={() => Router.back()} className="back-button">
              &#x2b05;
            </span>
          )}
          <Link href="">
            <span className="main-title">Hacker TypeNext</span>
          </Link>
        </nav>
      </header>
      <main>{children}</main>
      <footer />
      <style jsx={true}>{`
        .container {
          max-width: 800px;
          margin: 0 auto;
          background: #f6f6ef;
        }
        nav {
          background: #f60;
          padding: 1em;
        }
        nav > * {
          display: inline-block;
          color: black;
        }
        nav a {
          text-decoration: none;
        }
        nav .main-title {
          font-weight: bold;
        }
        nav .back-button {
          font-size: 0.9rem;
          padding-right: 1em;
          cursor: pointer;
        }
      `}</style>

      <style global={true} jsx={true}>{`
        body {
          background: white;
          font-family: Verdana, Geneva, sans-serif;
      `}</style>
    </div>
  )
}
