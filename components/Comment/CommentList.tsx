import React from 'react'
import Comment from './Comment'
type CommentListProps = {
  comments: Array<any>
}

export default function CommentList({ comments }: CommentListProps) {
  return (
    <>
      {comments.map((comment) => (
        <Comment key={comment.id} comment={comment} />
      ))}
    </>
  )
}
