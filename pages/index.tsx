import React, { Component } from 'react'
import fetch from 'isomorphic-unfetch'
import Error from 'next/error'
import StoryList from '../components/StoryList/StoryList'
import Layout from '../components/Layout/Layout'
import Link from 'next/link'

type Props = {
  stories: Array<any> | null
  page?: number
}

class HomePage extends Component<Props> {
  static getInitialProps = async ({ query }: { query: any }) => {
    console.log({ query })
    try {
      const page = Number(query.page) || 1
      const response = await fetch(
        `https://node-hnapi.herokuapp.com/news?page=${page || 1}`
      )
      const stories = await response.json()
      return { stories: stories || [], page }
    } catch (e) {
      return { stories: null }
    }
  }

  render() {
    const { stories, page = 1 } = this.props
    if (stories === null) return <Error statusCode={503} />
    return (
      <Layout
        title="Hacker TypeNext"
        description="A hackernews clone made with Typescript and React NextJS"
      >
        <StoryList stories={stories} />
        <footer>
          <Link href={`/?page=${page + 1}`}>
            <a>{`Next Page (${page + 1})`}</a>
          </Link>
        </footer>

        <style jsx={true}>{`
          footer {
            padding: 1em;
          }
          footer a {
            font-weight: bold;
            color: black;
            text-decoration: none;
          }
        `}</style>
      </Layout>
    )
  }
}

export default HomePage
